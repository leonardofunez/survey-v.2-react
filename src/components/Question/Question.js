import React, { useState } from 'react'
import PropTypes from 'prop-types'
import './Question.scss'

import store from '../../redux/store'
import { VOTE } from '../../redux/creators'

const Question = props => {
  const [like, setLike] = useState(null)
  // To compare the state (like) to activate the button selected 
  // We use === to change the buttons state in the markup
  // because this variable has three states: null(both buttons unselected) as an initial state, true(Yes/Like selected) and false(No/Dislike selected)

  const saveVote = (value) => {
    // Creating an object with the question answered
    const question = {index: props.questionIndex, answer: value}

    // Getting current Store
    let currentState = store.getState().votes
    
    // Finding new question in Store
    const findQuestion = currentState.find(item => item.index === question.index)
    
    if( findQuestion === undefined ){
      // New question
      currentState = [...currentState, question]
    }else{
      // Updating a question already answered
      const indexToUpdate = currentState.findIndex(item => item.index === question.index)
      currentState[indexToUpdate].answer = value
    }

    // Saving in store
    store.dispatch(VOTE(currentState))

    // Changing Like button state
    setLike(value)
  }

  return(
    <div className='question align-center is-flex flex-column flex-justify-center flex-between is-relative' data-visible={props.visible}>
      <h2 className='question__category f-weight-700 f-24'>{props.category}</h2>
      
      <p className='question__description f-16'>{props.description}</p>
      
      <div className='like-buttons is-flex flex-align-center flex-between'>
        <button className='like-button like-button__dislike' data-active={'true' ? like === false : 'false'} onClick={() => saveVote(false)}>
          <span className='like-button__icon'></span>
        </button>
        <button className='like-button' data-active={'true' ? like === true : 'false'} onClick={() => saveVote(true)}>
          <span className='like-button__icon'></span>
        </button>
      </div>
    </div>
  )
}

Question.propTypes = {
  questionIndex : PropTypes.number.isRequired,
  visible : PropTypes.bool.isRequired,
  category : PropTypes.string.isRequired,
  description: PropTypes.object.isRequired
}

export default Question