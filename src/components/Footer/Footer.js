import React from 'react'
import { NavLink } from 'react-router-dom'
import './Footer.scss'

const Footer = () => {
  return(
    <footer className='main-footer'>
      <div className='wrapper is-flex flex-between flex-align-end f-12'>
        <div className='logo-copyright is-flex flex-column'>
          <NavLink to='/' className='logo'></NavLink>
          <span className='rigths'>© 2019 Survey App. All rights reserved.</span>
        </div>
        <div className='author'>Designed and Developed by <a className='color-red' href='http://leonardofunez.com' target='_blank' rel='noopener noreferrer'>Leonardo Funez</a></div>
      </div>
    </footer>
  )
}

export default Footer