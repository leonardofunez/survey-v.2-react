import React from 'react'
import ReactDOM from 'react-dom'
import './assets/scss/main.scss'

import store from './redux/store'
import { Provider } from 'react-redux'

import Routes from './config/routes'
import * as serviceWorker from './serviceWorker'

ReactDOM.render(
  <Provider store={store}>{Routes}</Provider>,
document.getElementById('root'))
serviceWorker.unregister();
