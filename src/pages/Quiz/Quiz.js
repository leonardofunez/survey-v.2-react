import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom'

import renderHTML from '../../config/renderHTML'
import APISurvey from '../../config/api'
import store from '../../redux/store'
import { IS_LOADING, ORG_QUESTIONS } from '../../redux/creators'

import Question from '../../components/Question/Question'

const Survey = () => {
  const [questions, setQuestions] = useState([]),
        [currentQuestion, setCurrentQuestion] = useState(1),
        [totalQuestions, setTotalQuestions] = useState(0),
        [isLoading, setIsLoading] = useState(true),
        [showMessage, setShowMessage] = useState(false)

  const getSurveys = async () => {
    // Getting the questions from the API
    const response = await APISurvey.getSurveys()
    
    // Saving the questions on the component state (page state) to iterate below (return)
    setQuestions(response)

    // We need this number to void move forward to the next question after the last. 
    // Also, it helps us show the total of questions below (return)
    setTotalQuestions(response.length)

    // Saving the original questions to compare them with the user answers.
    store.dispatch(ORG_QUESTIONS(response))

    setTimeout(() => {
      // Turning off the loading
      store.dispatch(IS_LOADING(false))

      // This state variable will help us show the content when Loading finished
      setIsLoading(false)
    }, 1000)
  }

  // Changing the question through previous and next buttons.
  // If we are in the first question the previous button will be disabled.
  // If we are in the last question the Next button will be disabled. Even though the Send button will appear instead.
  const changeQuestion = (nextPrevQuestion) => {
    if( (nextPrevQuestion > 0) && (nextPrevQuestion <= totalQuestions) ) setCurrentQuestion(nextPrevQuestion)
  }

  // Avoid redirect to Result page if all questions weren't answered.
  // However, the Send button will send if all questions were answered.
  const sendSurvey = (e) => {
    if( store.getState().votes.length !== 10 ){
      e.preventDefault()
      setShowMessage(true)

      setTimeout(() => setShowMessage(false), 2500)
    }else{
      setShowMessage(false)
    }
  }

  useEffect(() => {
    // Setting initial State for original questions
    store.dispatch(ORG_QUESTIONS([]))
    
    // Turning on the loading
    store.dispatch(IS_LOADING(true))

    getSurveys()
  }, [])

  return(
    <section className='page page__survey'>
      { !isLoading ?
        <div className='wrapper'>
          {questions.map((item, index) => 
            <Question
              key={index}
              questionIndex={index+1}
              category={item.category}
              description={renderHTML(item.question)}
              visible={currentQuestion === index+1 ? true : false}
            />
          )}

          <div className='w-100 is-relative'>
            { showMessage ?
              <div className='nav-message align-center is-absolute w-100 f-12 f-weight-600 color-red'>To see the result you must complete all the quiz!</div>
            : null }

            <div className='nav-buttons is-flex flex-align-center flex-between is-relative w-100'>
              <div 
                className='button button__icon button__transp button__arrow button--to-rigth'
                onClick={() => changeQuestion(currentQuestion - 1)}
                data-active={'true' ? currentQuestion > 1 : 'false'}>
                  Previous
              </div>
              
              <div
                className='current-survey color-dark-gray f-weight-600 f-12'>
                  {currentQuestion} / {totalQuestions}
              </div>
              
              {currentQuestion < 10 ?
                <div
                  className='button button__icon button__arrow'
                  onClick={() => changeQuestion(currentQuestion + 1)}>
                    Next
                </div>
              :
                <NavLink
                  to='/results'
                  className='button button__icon button__send'
                  onClick={(e) => sendSurvey(e)}>
                    Send
                </NavLink>
              }
            </div>
          </div>
        </div>
      : null }
    </section>
  )
}

export default Survey