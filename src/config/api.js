import axios from 'axios'

export default {
  async getSurveys() {
    try {
      const response = await axios.get(`https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean`)
      return response.data.results
    }
    catch (error) {
      console.log(error)
    }
  }
}