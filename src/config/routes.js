import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Header from '../components/Header/Header'
import Footer from '../components/Footer/Footer'
import Loader from '../components/Loader/Loader'

import Home from '../pages/Home/Home'
import Quiz from '../pages/Quiz/Quiz'
import Results from '../pages/Results/Results'

const routes = (
  <Router>
    <React.Fragment>
      <main className='main'>
        <Loader />
        <Route path={['/quiz', '/results']} component={Header} />
        
        <div className='container'>
          <Route path='/' component={Home} exact />
          <Route path='/quiz' component={Quiz} exact />
          <Route path='/results' component={Results} exact />
        </div>
        
        <Route path={['/quiz', '/results']} component={Footer} />
      </main>
    </React.Fragment>
  </Router>
)

export default routes