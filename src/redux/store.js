import { createStore, applyMiddleware, combineReducers } from 'redux'
import * as reducers from './reducers'

// Middleware
  const logger = store => next => action => {
    // console.log('1. Dispatching', action)
    let result = next(action)
    // console.log('2. Next State', store.getState())
    return result
  }
// .Middleware

export default createStore(
  combineReducers(reducers),
  applyMiddleware(logger)
);