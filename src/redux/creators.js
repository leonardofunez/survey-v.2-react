export const ORG_QUESTIONS = value => {
  return {
    type: 'ORG_QUESTIONS',
    org_questions: value
  }
}

export const VOTE = value => {
  return {
    type: 'VOTE',
    votes: value
  }
}

export const IS_LOADING = state => {
  return {
    type: 'IS_LOADING',
    is_loading: state
  }
}